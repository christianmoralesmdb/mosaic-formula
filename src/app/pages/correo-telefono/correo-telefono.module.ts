import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CorreoTelefonoPageRoutingModule } from './correo-telefono-routing.module';

import { CorreoTelefonoPage } from './correo-telefono.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorreoTelefonoPageRoutingModule
  ],
  declarations: [CorreoTelefonoPage]
})
export class CorreoTelefonoPageModule {}

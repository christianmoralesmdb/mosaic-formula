import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorreoTelefonoPage } from './correo-telefono.page';

const routes: Routes = [
  {
    path: '',
    component: CorreoTelefonoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorreoTelefonoPageRoutingModule {}

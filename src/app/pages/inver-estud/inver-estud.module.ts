import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InverEstudPageRoutingModule } from './inver-estud-routing.module';

import { InverEstudPage } from './inver-estud.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InverEstudPageRoutingModule
  ],
  declarations: [InverEstudPage]
})
export class InverEstudPageModule {}

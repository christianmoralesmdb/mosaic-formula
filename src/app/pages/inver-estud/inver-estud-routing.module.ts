import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InverEstudPage } from './inver-estud.page';

const routes: Routes = [
  {
    path: '',
    component: InverEstudPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InverEstudPageRoutingModule {}

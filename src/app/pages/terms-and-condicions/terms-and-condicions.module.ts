import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermsAndCondicionsPageRoutingModule } from './terms-and-condicions-routing.module';

import { TermsAndCondicionsPage } from './terms-and-condicions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsAndCondicionsPageRoutingModule
  ],
  declarations: [TermsAndCondicionsPage]
})
export class TermsAndCondicionsPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TermsAndCondicionsPage } from './terms-and-condicions.page';

const routes: Routes = [
  {
    path: '',
    component: TermsAndCondicionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TermsAndCondicionsPageRoutingModule {}

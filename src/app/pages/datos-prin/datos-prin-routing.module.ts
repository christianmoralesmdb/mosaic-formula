import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatosPrinPage } from './datos-prin.page';

const routes: Routes = [
  {
    path: '',
    component: DatosPrinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatosPrinPageRoutingModule {}

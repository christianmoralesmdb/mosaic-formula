import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatosPrinPageRoutingModule } from './datos-prin-routing.module';

import { DatosPrinPage } from './datos-prin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosPrinPageRoutingModule
  ],
  declarations: [DatosPrinPage]
})
export class DatosPrinPageModule {}

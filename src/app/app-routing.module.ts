import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',redirectTo:'inicio',pathMatch:'full'    
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'bienvenida',
    loadChildren: () => import('./pages/bienvenida/bienvenida.module').then( m => m.BienvenidaPageModule)
  },
  {
    path: 'datos-prin',
    loadChildren: () => import('./pages/datos-prin/datos-prin.module').then( m => m.DatosPrinPageModule)
  },
  {
    path: 'inver-estud',
    loadChildren: () => import('./pages/inver-estud/inver-estud.module').then( m => m.InverEstudPageModule)
  },
  {
    path: 'pais',
    loadChildren: () => import('./pages/pais/pais.module').then( m => m.PaisPageModule)
  },
  {
    path: 'terms-and-condicions',
    loadChildren: () => import('./pages/terms-and-condicions/terms-and-condicions.module').then( m => m.TermsAndCondicionsPageModule)
  },
  {
    path: 'validation',
    loadChildren: () => import('./pages/validation/validation.module').then( m => m.ValidationPageModule)
  },
  {
    path: 'correo-telefono',
    loadChildren: () => import('./pages/correo-telefono/correo-telefono.module').then( m => m.CorreoTelefonoPageModule)
  },
  {
    path: 'idioma',
    loadChildren: () => import('./pages/idioma/idioma.module').then( m => m.IdiomaPageModule)
  },
  {
    path: 'curso',
    loadChildren: () => import('./pages/curso/curso.module').then( m => m.CursoPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
